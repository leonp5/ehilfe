import React from "react";

export default function useThemeCheck() {
  const [toggle, setToggle] = React.useState();
  const activeTheme = localStorage.getItem("theme");

  React.useEffect(() => {
    if (activeTheme === "dark") {
      setToggle(true);
    } else setToggle(false);
  }, [activeTheme]);

  return [toggle, setToggle];
}
