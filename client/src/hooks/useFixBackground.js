import React from "react";

export default function useFixBackground() {
  const [toggle, setToggle] = React.useState(false);
  const [scrollBarWidth, setScrollBarWidth] = React.useState(false);

  async function handleToggle() {
    if (toggle === false) {
      const documentWidth = document.documentElement.clientWidth;
      const windowWidth = window.innerWidth;
      const sbWidth = windowWidth - documentWidth;
      setScrollBarWidth(sbWidth);
      document.body.style.overflow = "hidden";
      setToggle(!toggle);
    }
    if (toggle === true) {
      setToggle(!toggle);
      setScrollBarWidth(false);
      document.body.style.overflow = "visible";
    }
  }
  return [toggle, handleToggle, scrollBarWidth];
}
