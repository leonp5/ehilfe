import React from "react";

export default function useScrollPosition() {
  const [scrollPosition, setScrollPosition] = React.useState();

  React.useEffect(() => {
    function handleScroll() {
      setScrollPosition(window.pageYOffset);
    }
    window.addEventListener("scroll", handleScroll);
    return () => window.removeEventListener("scroll", handleScroll);
  }, [scrollPosition]);

  return scrollPosition;
}
