import React from "react";

export default function useMountCheck() {
  const [show, setShow] = React.useState(false);
  const theme = localStorage.getItem("theme");

  React.useEffect(() => {
    function checkStatus() {
      if (theme === "dark" || "light") {
        setShow(true);
      }
    }
    checkStatus();
  }, [theme]);

  return show;
}
