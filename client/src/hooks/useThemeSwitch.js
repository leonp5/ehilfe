import React from "react";

export default function useThemeSwitch() {
  const [theme, setTheme] = React.useState("light");

  function setMode(mode) {
    localStorage.setItem("theme", mode);
    setTheme(mode);
  }

  function toggleTheme() {
    if (theme === "light") {
      setMode("dark");
    } else {
      setMode("light");
    }
  }

  React.useEffect(() => {
    const localTheme = localStorage.getItem("theme");
    if (localTheme) {
      setTheme(localTheme);
    } else {
      setMode("light");
    }
  }, []);

  return [theme, toggleTheme];
}
