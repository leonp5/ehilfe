export const light = {
  colors: {
    background: "#FFFFFF",
    primary: "#990000",
    secondary: "#033664",
    tertiary: "#bfbfbf",
    font: "#2e2e2e",
    action: "#1c7c54",
    warning: "red",
    transparentBackground: "rgba(0,0,0, 0.3)"
  }
};

export const dark = {
  colors: {
    background: "#222831",
    primary: "#990000",
    secondary: "#d9d9d9",
    tertiary: "#393e46",
    font: "#d9d9d9",
    action: "#FFD700",
    warning: "red",
    transparentBackground: "rgba(0,0,0, 0.3)"
  }
};
