import React from "react";
import useFixBackground from "../../hooks/useFixBackground";

export const ToggleModalContext = React.createContext();

export const ModalProvider = props => {
  const [toggle, handleToggle, sbWidth1] = useFixBackground();
  const [open, handleOpen, sbWidth2] = useFixBackground();
  const fix = open || toggle;
  const scrollBarWidth = sbWidth1 || sbWidth2;

  return (
    <ToggleModalContext.Provider
      value={{ fix, toggle, handleToggle, open, handleOpen, scrollBarWidth }}
    >
      {props.children}
    </ToggleModalContext.Provider>
  );
};
