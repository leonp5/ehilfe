import styled from "@emotion/styled";

export const H1 = styled.h1`
  color: ${props => props.theme.colors.primary};
  white-space: nowrap;
  grid-column: 1;
  font-size: 2.7rem;
  margin: 0;
  margin-left: 20px;
  width: 100%;
`;

export const H2 = styled.h2`
  text-align: left;
  width: 100%;
`;

export const H3 = styled.h3`
  text-align: center;
  width: 100%;
`;

export const DisclaimerHeading = styled.h3`
  text-align: left;
`;

export const H5 = styled.h5``;

export const Text = styled.p`
  position: relative;
`;

export const AdviceText = styled.p`
  color: ${props => props.theme.colors.warning};
`;

export const ColumnText = styled.p`
  white-space: nowrap;
  text-align: right;
`;

export const ModalText = styled.p`
  text-align: center;
`;

export const DisclaimerText = styled.p`
  text-align: left;
`;

export const ModalWarning = styled(AdviceText)`
  text-align: center;
`;

export const CheckBoxText = styled(ModalText)`
  font-size: 0.9rem;
`;

export const ButtonText = styled.p`
  color: ${props => props.theme.colors.font};
  margin: 0;
  padding: 0;
`;

export const MobileMenuText = styled.p``;
