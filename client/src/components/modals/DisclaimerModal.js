import styled from "@emotion/styled";
import React from "react";

import { CloseButton } from "../buttons/CloseButton";
import { ToggleModalContext } from "../contexts/ToggleModalContext";
import { H2, DisclaimerText, DisclaimerHeading } from "../Text";
import { InlineAnchor } from "../navigation/NavLinks";

export const ModalContent = styled.div`
  overflow: auto;
  width: 100%;
  height: 100%;
  padding: 30px;
  background: ${props => props.theme.colors.background};
  -webkit-box-shadow: 0px 0px 10px 5px #000000;
  box-shadow: 0px 0px 10px 5px #000000;
`;

const StickyContainer = styled.div`
  position: fixed;
  z-index: 2;
  margin: auto;
  left: 0;
  top: 0;
  bottom: 0;
  width: 80%;
  height: 80vh;
  transition: transform 0.3s ease-in-out;
  transform: ${({ open }) => (open ? "translateX(0)" : "translateX(-110%)")};
`;

export default function DisclaimerModal() {
  const { open, handleOpen } = React.useContext(ToggleModalContext);

  return (
    <StickyContainer open={open}>
      <CloseButton onClick={handleOpen} />

      <ModalContent>
        <H2>Impressum</H2>
        <H2>Angaben gemäß § 5 TMG</H2>
        <DisclaimerText>
          Leon Pelzer <br />
          Hahnenweg 1 <br />
          51061 Köln
        </DisclaimerText>
        <H2>Kontakt</H2>
        <DisclaimerText>
          E-Mail:{" "}
          <InlineAnchor href="mailto:leonpe@web.de">leonpe@web.de</InlineAnchor>
        </DisclaimerText>
        <DisclaimerHeading>Haftung für Inhalte</DisclaimerHeading>
        <DisclaimerText>
          Als Diensteanbieter bin ich gemäß § 7 Abs.1 TMG für eigene Inhalte auf
          diesen Seiten nach den allgemeinen Gesetzen verantwortlich. Nach §§ 8
          bis 10 TMG bin ich als Diensteanbieter jedoch nicht verpflichtet,
          übermittelte oder gespeicherte fremde Informationen zu überwachen oder
          nach Umständen zu forschen, die auf eine rechtswidrige Tätigkeit
          hinweisen. <br /> <br />
          Verpflichtungen zur Entfernung oder Sperrung der Nutzung von
          Informationen nach den allgemeinen Gesetzen bleiben hiervon unberührt.
          Eine diesbezügliche Haftung ist jedoch erst ab dem Zeitpunkt der
          Kenntnis einer konkreten Rechtsverletzung möglich. Bei Bekanntwerden
          von entsprechenden Rechtsverletzungen werde ich diese Inhalte umgehend
          entfernen.
        </DisclaimerText>
        <DisclaimerHeading>Haftung für Links</DisclaimerHeading>
        <DisclaimerText>
          Mein Angebot enthält Links zu externen Websites Dritter, auf deren
          Inhalte ich keinen Einfluss habe. Deshalb kann ich für diese fremden
          Inhalte auch keine Gewähr übernehmen. Für die Inhalte der verlinkten
          Seiten ist stets der jeweilige Anbieter oder Betreiber der Seiten
          verantwortlich. Die verlinkten Seiten wurden zum Zeitpunkt der
          Verlinkung auf mögliche Rechtsverstöße überprüft. Rechtswidrige
          Inhalte waren zum Zeitpunkt der Verlinkung nicht erkennbar. <br />{" "}
          <br />
          Eine permanente inhaltliche Kontrolle der verlinkten Seiten ist jedoch
          ohne konkrete Anhaltspunkte einer Rechtsverletzung nicht zumutbar. Bei
          Bekanntwerden von Rechtsverletzungen werden wir derartige Links
          umgehend entfernen.
        </DisclaimerText>
        <DisclaimerHeading>Urheberrecht</DisclaimerHeading>
        <DisclaimerText>
          Soweit die Inhalte auf dieser Seite nicht vom Betreiber erstellt
          wurden, werden die Urheberrechte Dritter beachtet. Insbesondere werden
          Inhalte Dritter als solche gekennzeichnet. Sollten Sie trotzdem auf
          eine Urheberrechtsverletzung aufmerksam werden, bitten wir um einen
          entsprechenden Hinweis. Bei Bekanntwerden von Rechtsverletzungen
          werden wir derartige Inhalte umgehend entfernen. <br /> <br />
          Quelle:{" "}
          <InlineAnchor
            href="https://www.e-recht24.de/impressum-generator.html"
            target="_blank"
            rel="noopener"
          >
            e-recht24.de
          </InlineAnchor>
        </DisclaimerText>
        <DisclaimerText>
          Originalfoto auf der Startseite:{" "}
          <InlineAnchor
            href="https://500px.com/jonvez"
            target="_blank"
            rel="noopener"
          >
            Jonathan Velasquez
          </InlineAnchor>{" "}
          <br />
          (Foto von mir geschnitten)
        </DisclaimerText>
      </ModalContent>
    </StickyContainer>
  );
}
