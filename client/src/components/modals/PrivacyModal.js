import styled from "@emotion/styled";
import React from "react";

import { H2, Text } from "../Text";
import { CloseButton } from "../buttons/CloseButton";

const ModalBackground = styled.div`
  position: absolute;
  z-index: 2;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background: ${props => props.theme.colors.transparentBackground};
  -webkit-transition: opacity 0.9s ease-in-out, visibility 0.4s ease-in-out;
  transition: opacity 0.9s ease-in-out, visibility 0.4s ease-in-out;
  visibility: ${({ toggle }) => (toggle ? "visible" : "hidden")};
  opacity: ${({ toggle }) => (toggle ? "1" : "0")};
`;

const ModalContent = styled.div`
  overflow: auto;
  width: 100%;
  height: 100%;
  padding: 40px;
  background: ${props => props.theme.colors.background};
  -webkit-box-shadow: 0px 0px 20px 5px #000000;
  box-shadow: 0px 0px 20px 5px #000000;
`;

const StickyContainer = styled.div`
  position: fixed;
  z-index: 3;
  width: 90%;
  height: 85vh;
  margin: auto;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  transition: transform 0.35s 0.1s ease-in-out;
  transform: ${({ toggle }) =>
    toggle ? "translateY(0%)" : "translateY(-120%)"};
`;

export default function PrivacyModal({ props }) {
  return (
    <>
      <ModalBackground toggle={props.toggle} />
      <StickyContainer toggle={props.toggle}>
        <CloseButton onClick={props.handleToggle} />
        <ModalContent>
          <H2>Datenschutz</H2>

          <Text>
            <strong>Allgemeiner Hinweis und Pflichtinformationen</strong>
          </Text>
          <Text>
            <strong>Benennung der verantwortlichen Stelle</strong>
          </Text>
          <Text>
            Die verantwortliche Stelle für die Datenverarbeitung auf dieser
            Website ist:
          </Text>
          <Text>
            <br />
            <span>Leon Pelzer</span>
            <br />
            <span>Hahnenweg 1</span>
            <br />
            <span>51061</span> <span>Köln</span>
          </Text>
          <Text>
            Die verantwortliche Stelle entscheidet allein oder gemeinsam mit
            anderen über die Zwecke und Mittel der Verarbeitung von
            personenbezogenen Daten (z.B. Namen, Kontaktdaten o. Ä.).
          </Text>

          <Text>
            <strong>Widerruf Ihrer Einwilligung zur Datenverarbeitung</strong>
          </Text>
          <Text>
            Nur mit Ihrer ausdrücklichen Einwilligung sind einige Vorgänge der
            Datenverarbeitung möglich. Ein Widerruf Ihrer bereits erteilten
            Einwilligung ist jederzeit möglich. Für den Widerruf genügt eine
            formlose Mitteilung per E-Mail. Die Rechtmäßigkeit der bis zum
            Widerruf erfolgten Datenverarbeitung bleibt vom Widerruf unberührt.
          </Text>

          <Text>
            <strong>
              Recht auf Beschwerde bei der zuständigen Aufsichtsbehörde
            </strong>
          </Text>
          <Text>
            Als Betroffener steht Ihnen im Falle eines datenschutzrechtlichen
            Verstoßes ein Beschwerderecht bei der zuständigen Aufsichtsbehörde
            zu. Zuständige Aufsichtsbehörde bezüglich datenschutzrechtlicher
            Fragen ist der Landesdatenschutzbeauftragte des Bundeslandes, in dem
            sich der Sitz unseres Unternehmens befindet. Der folgende Link
            stellt eine Liste der Datenschutzbeauftragten sowie deren
            Kontaktdaten bereit:{" "}
            <a
              href="https://www.bfdi.bund.de/DE/Infothek/Anschriften_Links/anschriften_links-node.html"
              target="_blank"
              rel="noopener noreferrer"
            >
              https://www.bfdi.bund.de/DE/Infothek/Anschriften_Links/anschriften_links-node.html
            </a>
            .
          </Text>

          <Text>
            <strong>Recht auf Datenübertragbarkeit</strong>
          </Text>
          <Text>
            Ihnen steht das Recht zu, Daten, die wir auf Grundlage Ihrer
            Einwilligung oder in Erfüllung eines Vertrags automatisiert
            verarbeiten, an sich oder an Dritte aushändigen zu lassen. Die
            Bereitstellung erfolgt in einem maschinenlesbaren Format. Sofern Sie
            die direkte Übertragung der Daten an einen anderen Verantwortlichen
            verlangen, erfolgt dies nur, soweit es technisch machbar ist.
          </Text>

          <Text>
            <strong>
              Recht auf Auskunft, Berichtigung, Sperrung, Löschung
            </strong>
          </Text>
          <Text>
            Sie haben jederzeit im Rahmen der geltenden gesetzlichen
            Bestimmungen das Recht auf unentgeltliche Auskunft über Ihre
            gespeicherten personenbezogenen Daten, Herkunft der Daten, deren
            Empfänger und den Zweck der Datenverarbeitung und ggf. ein Recht auf
            Berichtigung, Sperrung oder Löschung dieser Daten. Diesbezüglich und
            auch zu weiteren Fragen zum Thema personenbezogene Daten können Sie
            sich jederzeit über die im Impressum aufgeführten
            Kontaktmöglichkeiten an uns wenden.
          </Text>

          <Text>
            <strong>SSL- bzw. TLS-Verschlüsselung</strong>
          </Text>
          <Text>
            Aus Sicherheitsgründen und zum Schutz der Übertragung vertraulicher
            Inhalte, die Sie an uns als Seitenbetreiber senden, nutzt unsere
            Website eine SSL-bzw. TLS-Verschlüsselung. Damit sind Daten, die Sie
            über diese Website übermitteln, für Dritte nicht mitlesbar. Sie
            erkennen eine verschlüsselte Verbindung an der „https://“
            Adresszeile Ihres Browsers und am Schloss-Symbol in der
            Browserzeile.
          </Text>

          <Text>
            <strong>Server-Log-Dateien</strong>
          </Text>
          <Text>
            In Server-Log-Dateien erhebt und speichert der Provider der Website
            automatisch Informationen, die Ihr Browser automatisch an uns
            übermittelt. Dies sind:
          </Text>
          <ul>
            <li>Besuchte Seite auf unserer Domain</li>
            <li>Datum und Uhrzeit der Serveranfrage</li>
            <li>Browsertyp und Browserversion</li>
            <li>Verwendetes Betriebssystem</li>
            <li>Referrer URL</li>
            <li>Hostname des zugreifenden Rechners</li>
            <li>IP-Adresse</li>
          </ul>
          <Text>
            Es findet keine Zusammenführung dieser Daten mit anderen
            Datenquellen statt. Grundlage der Datenverarbeitung bildet Art. 6
            Abs. 1 lit. b DSGVO, der die Verarbeitung von Daten zur Erfüllung
            eines Vertrags oder vorvertraglicher Maßnahmen gestattet.
          </Text>

          <Text>
            <strong>Kontaktformular</strong>
          </Text>
          <Text>
            Per Kontaktformular übermittelte Daten werden einschließlich Ihrer
            Kontaktdaten gespeichert, um Ihre Anfrage bearbeiten zu können oder
            um für Anschlussfragen bereitzustehen. Eine Weitergabe dieser Daten
            findet ohne Ihre Einwilligung nicht statt.
          </Text>
          <Text>
            Die Verarbeitung der in das Kontaktformular eingegebenen Daten
            erfolgt ausschließlich auf Grundlage Ihrer Einwilligung (Art. 6 Abs.
            1 lit. a DSGVO). Ein Widerruf Ihrer bereits erteilten Einwilligung
            ist jederzeit möglich. Für den Widerruf genügt eine formlose
            Mitteilung per E-Mail. Die Rechtmäßigkeit der bis zum Widerruf
            erfolgten Datenverarbeitungsvorgänge bleibt vom Widerruf unberührt.
          </Text>
          <Text>
            Über das Kontaktformular übermittelte Daten verbleiben bei uns, bis
            Sie uns zur Löschung auffordern, Ihre Einwilligung zur Speicherung
            widerrufen oder keine Notwendigkeit der Datenspeicherung mehr
            besteht. Zwingende gesetzliche Bestimmungen - insbesondere
            Aufbewahrungsfristen - bleiben unberührt.
          </Text>
          <Text>
            <small>
              Quelle: Datenschutz-Konfigurator von{" "}
              <a
                href="http://www.mein-datenschutzbeauftragter.de"
                target="_blank"
                rel="noopener noreferrer"
              >
                mein-datenschutzbeauftragter.de
              </a>
            </small>
          </Text>
        </ModalContent>
      </StickyContainer>
    </>
  );
}
