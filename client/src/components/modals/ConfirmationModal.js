import styled from "@emotion/styled";
import React from "react";

const ModalBackground = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  position: absolute;
  z-index: 2;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background: ${props => props.theme.colors.transparentBackground};

  animation: show 0.3s;

  @keyframes show {
    from {
      transform: scale(0);
      opacity: 0;
      z-index: -1;
    }
    to {
      transform: scale(1);
      opacity: 1;
      z-index: 2;
    }
  }
`;

const ModalContent = styled.div`
  margin: 20px;
  padding: 40px;
  background: ${props => props.theme.colors.background};
  -webkit-box-shadow: 0px 0px 20px 5px #000000;
  box-shadow: 0px 0px 20px 5px #000000;
`;

export default function ConfirmationModal({ children }) {
  return (
    <>
      <ModalBackground>
        <ModalContent>{children}</ModalContent>
      </ModalBackground>
    </>
  );
}
