import styled from "@emotion/styled";
import React from "react";

import { ToggleModalContext } from "../contexts/ToggleModalContext";

const Container = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  margin: 0;
  height: 100%;
  width: 100%;
  filter: ${({ fix }) => (fix ? "blur(2px)" : "")};
`;

export default function BlurredBackground({ children }) {
  const { open } = React.useContext(ToggleModalContext);

  return <Container fix={open}>{children}</Container>;
}
