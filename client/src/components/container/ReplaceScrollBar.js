import styled from "@emotion/styled";
import React from "react";

import { ToggleModalContext } from "../contexts/ToggleModalContext";

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  width: 100%;
  height: 100%;
  padding-right: ${({ scrollBarWidth }) =>
    scrollBarWidth ? `${scrollBarWidth}px` : "0px"};
`;

export function ReplaceScrollBar({ children }) {
  const { scrollBarWidth } = React.useContext(ToggleModalContext);

  return <Container scrollBarWidth={scrollBarWidth}>{children}</Container>;
}
