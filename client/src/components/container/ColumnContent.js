import styled from "@emotion/styled";

export const ColumnContent = styled.div`
  display: flex;
  margin-left: 30px;
  margin-top: 70px;
`;
