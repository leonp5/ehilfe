import React from "react";
import styled from "@emotion/styled";

import ToggleTheme from "../buttons/ToggleTheme";
import { H1 } from "../Text";
import { HeaderNav } from "../navigation/HeaderNav";
import { mq } from "../MediaQueries";
import BurgerButton from "../buttons/BurgerButton";
import MobileMenu from "../navigation/MobileMenu";
import { HeaderLink } from "../navigation/NavLinks";
import useOnClickOutside from "../../hooks/useOnClickOutside";
import { BasicLink } from "../navigation/NavLinks";
import { ToggleModalContext } from "../contexts/ToggleModalContext";

const Container = styled.div`
  z-index: 1;
  display: grid;
  grid-template-columns: 1fr 1fr 2fr;
  grid-template-rows: 1fr;
  align-items: center;
  position: sticky;
  top: -5px;
  height: 5rem;
  width: 100%;
  background: ${props => props.theme.colors.tertiary};
  box-shadow: 0px 5px 5px 0px rgba(0, 0, 0, 0.2);
  padding-right: ${({ scrollBarWidth }) =>
    scrollBarWidth ? `${scrollBarWidth}px` : "0px"};
`;

const DesktopContainer = styled.div`
  ${mq[0]} {
    display: none;
  }
  grid-column: 3 / 4;
  height: 100%;
  width: 100%;
  display: grid;
  grid-template-columns: 430px 1fr 60px 80px;
  grid-template-rows: 1fr;
  align-items: center;
`;

export default function Header() {
  const [open, setOpen] = React.useState(false);
  const node = React.useRef();
  useOnClickOutside(node, () => setOpen(false));
  const { scrollBarWidth } = React.useContext(ToggleModalContext);

  return (
    <Container scrollBarWidth={scrollBarWidth} ref={node}>
      <BasicLink to="/">
        <H1>e-Hilfe</H1>
      </BasicLink>
      <DesktopContainer>
        <HeaderNav>
          <HeaderLink to="/" exact={true}>
            E-HILFE
          </HeaderLink>
          <HeaderLink to="/idea">IDEE</HeaderLink>
          <HeaderLink to="/payment">BEZAHLUNG?</HeaderLink>
          <HeaderLink to="/contact">KONTAKT</HeaderLink>
        </HeaderNav>
        <ToggleTheme />
      </DesktopContainer>
      <BurgerButton open={open} onClick={() => setOpen(!open)} />
      <MobileMenu open={open} onClick={() => setOpen()}></MobileMenu>
    </Container>
  );
}
