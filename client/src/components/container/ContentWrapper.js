import styled from "@emotion/styled";

import { mq } from "../MediaQueries";

export const ContentWrapper = styled.div`
  width: 100%;
  max-width: 1000px;
  display: flex;
  justify-content: center;
  ${mq[1]} {
    flex-flow: row wrap;
  }
`;
