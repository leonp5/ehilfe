import styled from "@emotion/styled";
import React from "react";

import { CheckBox } from "../form/Inputs";
import { CheckBoxText } from "../Text";
import { InlineAnchor } from "../navigation/NavLinks";
import PrivacyModal from "../modals/PrivacyModal";
import { ToggleModalContext } from "../contexts/ToggleModalContext";

export const Container = styled.div`
  display: flex;
`;

export default function CheckBoxContainer(isChecked, setIsChecked) {
  const { toggle, handleToggle } = React.useContext(ToggleModalContext);

  return (
    <Container>
      <CheckBox
        type="checkbox"
        defaultChecked={isChecked}
        onChange={() => setIsChecked(!isChecked)}
      />{" "}
      <CheckBoxText>
        Ich habe die{" "}
        <InlineAnchor onClick={handleToggle}>
          Datenschutzbestimmungen
        </InlineAnchor>{" "}
        zur Kenntnis genommen.
      </CheckBoxText>
      <PrivacyModal props={{ toggle, handleToggle }} />
    </Container>
  );
}
