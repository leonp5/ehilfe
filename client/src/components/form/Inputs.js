import styled from "@emotion/styled";

export const InputField = styled.input`
  width: 15rem;
  height: 3rem;
  background: ${props => props.theme.colors.background};
  color: ${props => props.theme.colors.font};
  border: solid 1px ${props => props.theme.colors.font};
  margin-bottom: 20px;
  box-shadow: inset 0 0px 5px 0 hsla(0, 0%, 0%, 0.4);
  font-size: 100%;
  -webkit-transition: all 0.3s ease-in-out;
  -moz-transition: all 0.3s ease-in-out;
  -ms-transition: all 0.3s ease-in-out;
  -o-transition: all 0.3s ease-in-out;

  :hover {
    border: solid 1px ${props => props.theme.colors.action};
  }
  :focus {
    box-shadow: 0 0 5px ${props => props.theme.colors.action};
    border: solid 1px ${props => props.theme.colors.action};
  }
`;

export const TextArea = styled.textarea`
  min-width: 320px;
  height: 10rem;
  background: ${props => props.theme.colors.background};
  color: ${props => props.theme.colors.font};
  border: solid 1px ${props => props.theme.colors.font};
  box-shadow: inset 0 0px 5px 0 hsla(0, 0%, 0%, 0.4);
  font-size: 120%;

  -webkit-transition: all 0.3s ease-in-out;
  -moz-transition: all 0.3s ease-in-out;
  -ms-transition: all 0.3s ease-in-out;
  -o-transition: all 0.3s ease-in-out;

  :hover {
    border: solid 1px ${props => props.theme.colors.action};
  }
  :focus {
    box-shadow: 0 0 5px ${props => props.theme.colors.action};
    border: solid 1px ${props => props.theme.colors.action};
  }
`;

export const CheckBox = styled.input`
  margin-right: 10px;
  :focus {
    outline: none;
  }
  ::-moz-focus-inner {
    border: 0;
  }
`;
