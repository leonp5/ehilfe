import styled from "@emotion/styled";

const Image = styled.img`
  width: 40%;
  margin-left: 20px;
  margin-right: 20px;
`;

export const RightImage = styled(Image)`
  float: right;
`;

export const LeftImage = styled(Image)`
  float: left;
`;
