import styled from "@emotion/styled";

import { mq } from "../MediaQueries";

export const ContactButton = styled.button`
  grid-column: 1 / 2;
  width: 5.5rem;
  height: 2.5rem;
  border: none;
  cursor: pointer;
  text-decoration: none;
  border-radius: 4px;
  background: ${props => props.theme.colors.action};
  color: ${props => props.theme.colors.background};
  box-shadow: 0 0px 7px rgba(0, 0, 0, 0.2), 0 0px 15px rgba(0, 0, 0, 0.5);
  transition: 0.2s;
  :focus {
    outline: none;
  }
  ::-moz-focus-inner {
    border: 0;
  }
  :hover {
    box-shadow: 0 0px 7px rgba(0, 0, 0, 0.2), 0 0px 15px 5px rgba(0, 0, 0, 0.4);
  }
  :active {
    transition: 0.1s;
    box-shadow: 0 0px 7px rgba(0, 0, 0, 0.2), 0 0px 15px rgba(0, 0, 0, 0.5);
  }
`;

export const CenterButton = styled.div`
  position: absolute;
  display: grid;
  justify-items: center;
  grid-template-columns: 1fr 42%;
  grid-template-rows: 1fr;
  width: 100%;

  ${mq[3]} {
    grid-template-columns: 1fr;
  }
`;

export const CenterButtonBottom = styled.div`
  position: absolute;
  display: grid;
  justify-items: center;
  grid-template-columns: 42% 1fr;
  grid-template-rows: 1fr;
  width: 100%;

  ${mq[2]} {
    grid-template-columns: 1fr;
  }
`;
