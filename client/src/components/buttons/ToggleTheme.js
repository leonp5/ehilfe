import React from "react";
import styled from "@emotion/styled";

import useThemeCheck from "../../hooks/useThemeCheck";
import useMountCheck from "../../hooks/useMountCheck";
import { mq } from "../MediaQueries";
import { useContext } from "react";
import { ToggleThemeContext } from "../contexts/ToggleThemeContext";

const SwitchContainer = styled.label`
  grid-column: 4 / 5;
  position: relative;
  width: 60.8px;
  height: 35px;
  border-radius: 35px;
  display: inline-block;

  ${mq[0]} {
    height: 20px;
    width: 49px;
  }
`;

const StyledInput = styled.input`
  position: absolute;
  z-index: -1;
  opacity: 0;
  :checked + span {
    background-color: ${props => props.theme.colors.background};
  }

  :checked + span:before {
    background-color: ${props => props.theme.colors.action};
    box-shadow: -3px 0px 5px ${props => props.theme.colors.action};
    -webkit-transform: translateX(24px);
    -ms-transform: translateX(24px);
    transform: translateX(24px);
  }
`;

const Switch = styled.span`
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: ${props => props.theme.colors.background};
  -webkit-transition: 0.4s;
  transition: 0.4s;
  border-radius: 35px;

  :before {
    position: absolute;
    content: "";
    height: 29px;
    width: 29px;
    left: 4px;
    bottom: 3px;
    background-color: ${props => props.theme.colors.action};
    -webkit-transition: 0.4s;
    transition: 0.4s;
    border-radius: 35px;
    box-shadow: 2px 0px 5px #aaa;

    ${mq[0]} {
      height: 22px;
      width: 22px;
      bottom: -1px;
      left: 1px;
    }
  }
`;

export default function ToggleTheme() {
  const [toggle, setToggle] = useThemeCheck();
  const show = useMountCheck();
  const toggleTheme = useContext(ToggleThemeContext);

  return (
    <>
      {show && (
        <SwitchContainer>
          <StyledInput
            type="checkbox"
            onClick={toggleTheme}
            defaultChecked={toggle}
            onChange={() => setToggle(!toggle)}
          />
          <Switch></Switch>
        </SwitchContainer>
      )}
    </>
  );
}
