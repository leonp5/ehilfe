import styled from "@emotion/styled";

export const CloseButton = styled.a`
  position: absolute;
  top: 20px;
  right: 20px;
  width: 20px;
  height: 20px;
  opacity: 0.7;
  ::-moz-focus-inner {
    border: 0;
  }
  :hover {
    cursor: pointer;
    opacity: 1;
  }
  :before,
  :after {
    position: absolute;
    content: " ";
    height: 25px;
    width: 3px;
    background-color: ${props => props.theme.colors.action};
  }
  :before {
    transform: rotate(45deg);
  }
  :after {
    transform: rotate(-45deg);
  }
`;
