import styled from "@emotion/styled";
import React from "react";

import { ButtonText } from "../Text";
import DisclaimerModal from "../modals/DisclaimerModal";
import { ToggleModalContext } from "../contexts/ToggleModalContext";
import useScrollPosition from "../../hooks/useScrollPosition";

export const Button = styled.button`
  height: 25px;
  width: 7rem;
  text-align: center;
  border-radius: 9px 9px 0px 0px;
  border: solid 1px ${props => props.theme.colors.font};
  border-bottom: 0;
  position: sticky;
  bottom: 0px;
  margin-bottom: 60px;
  left: 10px;
  background: ${props => props.theme.colors.tertiary};
  cursor: pointer;

  :hover {
    opacity: 1;
  }
  :focus {
    outline: none;
  }
  ::-moz-focus-inner {
    border: 0;
  }

  -webkit-transition: opacity 0.9s, visibility 1s, transform 1.5s ease-in-out;
  transition: opacity 0.9s, visibility 1s, transform 1.5s ease-in-out;
  opacity: ${({ show }) => (show ? "0.8" : "0")};
  visibility: ${({ show }) => (show ? "visible" : "hidden")};
  transform: ${({ show }) => (show ? "translateY(0%)" : "translateY(-3300%)")};
  display: ${({ fix }) => (fix ? "none" : "")};
`;

export default function Disclaimer() {
  const scrollPosition = useScrollPosition();
  const [show, setShow] = React.useState(false);
  const { handleOpen, fix } = React.useContext(ToggleModalContext);

  React.useEffect(() => {
    if (scrollPosition > 300) {
      setShow(true);
    }
    if (scrollPosition < 219) {
      setShow(false);
    }
  }, [scrollPosition]);

  return (
    <>
      <Button show={show} fix={fix} onClick={handleOpen}>
        <ButtonText>Impressum</ButtonText>
      </Button>
      <DisclaimerModal />
    </>
  );
}
