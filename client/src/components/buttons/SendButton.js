import styled from "@emotion/styled";

import { ContactButton } from "./ContactButton";

export const SendButton = styled(ContactButton)`
  :disabled {
    opacity: 0.7;
  }
`;

export const CenterSendButton = styled.div`
  display: flex;
  justify-content: center;
  width: 100%;
`;
