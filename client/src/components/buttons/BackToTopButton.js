import React from "react";
import styled from "@emotion/styled";

import useScrollPosition from "../../hooks/useScrollPosition";

const StyledButton = styled.button`
  display: flex;
  align-items: center;
  justify-content: center;
  position: fixed;
  right: 15px;
  bottom: 40px;
  height: 35px;
  width: 35px;
  border: none;
  z-index: 2;
  border-radius: 10%;
  background: ${props => props.theme.colors.action};
  cursor: pointer;
  :focus {
    outline: none;
  }
  ::-moz-focus-inner {
    border: 0;
  }

  :hover {
    -webkit-transition: 0.2s ease-in-out;
    transition: 0.2s ease-in-out;
    opacity: 1;
  }

  i {
    border: solid ${props => props.theme.colors.tertiary};
    border-width: 0px 4px 4px 0px;
    padding: 5px;
    margin-top: 5px;
    transform: rotate(-135deg);
  }
  -webkit-transition: 0.7s ease-in-out;
  transition: 0.7s ease-in-out;
  opacity: ${({ show }) => (show ? "0.8" : "0")};
  visibility: ${({ show }) => (show ? "visible" : "hidden")};
`;

export default function BackToTopButton() {
  const scrollPositon = useScrollPosition();
  const [showButton, setShowButton] = React.useState();

  React.useEffect(() => {
    if (scrollPositon > 400) {
      setShowButton(true);
    }
    if (scrollPositon < 419) {
      setShowButton(false);
    }
  }, [scrollPositon]);

  function ScrollUp() {
    window.scroll({
      top: 0,
      left: 0,
      behavior: "smooth"
    });
  }

  return (
    <StyledButton onClick={ScrollUp} show={showButton}>
      <i />
    </StyledButton>
  );
}
