const breakpoints = [770, 850, 1080, 1174];

export const mq = breakpoints.map(bp => `@media (max-width: ${bp}px)`);
