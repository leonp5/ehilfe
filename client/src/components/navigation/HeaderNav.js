import styled from "@emotion/styled";

export const HeaderNav = styled.nav`
  grid-column: 1 / 2;
  display: flex;
  justify-content: space-around;
  width: 100%;
`;
