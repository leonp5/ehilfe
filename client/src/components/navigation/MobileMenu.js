import React from "react";
import styled from "@emotion/styled";

import { mq } from "../MediaQueries";
import { MobileMenuLink } from "./NavLinks";
import ToggleTheme from "../buttons/ToggleTheme";
import { MobileMenuText } from "../Text";
import useThemeCheck from "../../hooks/useThemeCheck";

const StyledMenu = styled.nav`
  display: none;
  ${mq[0]} {
    align-self: center;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: space-around;
    position: fixed;
    z-index: 1;
    height: 70%;
    width: 80%;
    top: 0;
    margin-left: 10%;
    transition: transform 0.35s ease-in-out;
    transform: ${({ open }) => (open ? "translateY(0%)" : "translateY(-110%)")};
    background: ${props => props.theme.colors.tertiary};
    box-shadow: 0px 5px 5px 3px rgba(0, 0, 0, 0.2);
  }
`;

const ToggleContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-around;
  height: 80px;
  width: 100%;
`;

export default function MobileMenu({ open, onClick }) {
  const [show] = useThemeCheck();

  return (
    <StyledMenu open={open}>
      <MobileMenuLink to="/" exact={true} onClick={onClick}>
        E-HILFE
      </MobileMenuLink>
      <MobileMenuLink to="/idea" onClick={onClick}>
        IDEE
      </MobileMenuLink>
      <MobileMenuLink to="/payment" onClick={onClick}>
        BEZAHLUNG?
      </MobileMenuLink>
      <MobileMenuLink to="/contact" onClick={onClick}>
        KONTAKT
      </MobileMenuLink>
      <ToggleContainer>
        {show && <MobileMenuText>Aktiviere Tagansicht</MobileMenuText>}
        {!show && <MobileMenuText>Aktiviere Nachtansicht</MobileMenuText>}
        <ToggleTheme />
      </ToggleContainer>
    </StyledMenu>
  );
}
