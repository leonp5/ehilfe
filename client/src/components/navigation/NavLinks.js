import styled from "@emotion/styled";
import { NavLink } from "react-router-dom";

import { mq } from "../MediaQueries";

export const BasicLink = styled(NavLink)`
  text-decoration: none;
  text-align: center;
  cursor: pointer;
  transition: color 0.2s linear;
  :focus {
    outline: none;
  }
  ::-moz-focus-inner {
    border: 0;
  }
`;

export const ButtonLink = styled(BasicLink)`
  grid-column: 2 / 3;
  ${mq[2]} {
    grid-column-start: 1;
  }
`;

export const MobileMenuLink = styled(BasicLink)`
  display: flex;
  align-items: center;
  justify-content: center;
  font-size: 1.5rem;
  width: 90%;
  height: 10%;
  color: ${props => props.theme.colors.secondary};
  &:hover,
  &.active {
    color: ${props => props.theme.colors.background};
    background: ${props => props.theme.colors.secondary};
  }
`;
export const HeaderLink = styled(BasicLink)`
  font-size: 1.2rem;
  padding-left: 15px;
  padding-right: 15px;
  color: ${props => props.theme.colors.secondary};
  white-space: nowrap;
  &:hover,
  &.active {
    color: ${props => props.theme.colors.background};
    background: ${props => props.theme.colors.secondary};
  }
`;

export const InlineLink = styled(BasicLink)`
  color: ${props => props.theme.colors.action};
  :hover {
    color: ${props => props.theme.colors.background};
    background: ${props => props.theme.colors.action};
  }
`;

// Anchor Links

export const InlineAnchor = styled.a`
  text-decoration: none;
  color: ${props => props.theme.colors.action};
  font-weight: bold;
  transition: 0.2s;
  cursor: pointer;
  :hover {
    color: ${props => props.theme.colors.background};
    background: ${props => props.theme.colors.action};
  }
  :focus {
    outline: none;
  }
  ::-moz-focus-inner {
    border: 0;
  }
`;
