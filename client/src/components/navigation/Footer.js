import styled from "@emotion/styled";

export const Footer = styled.footer`
  position: absolute;
  bottom: 0;
  width: 100%;
  height: 60px;
  background: ${props => props.theme.colors.tertiary};
`;
