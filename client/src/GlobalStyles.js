import React from "react";
import { Global, css } from "@emotion/core";
import { useTheme } from "emotion-theming";

export default function GlobalStyles() {
  const theme = useTheme();

  return (
    <Global
      styles={css`
        *,
        *:before,
        *:after {
          box-sizing: border-box;
        }
        @font-face {
          font-family: "ElaineSans";
          src: url("/fonts/ElaineSans.eot"); /* IE9 Compat Modes */
          src: url("/fonts/ElaineSans.eot?#iefix") format("embedded-opentype"),
            /* IE6-IE8 */ url("/fonts/ElaineSans.otf") format("opentype"),
            /* Open Type Font */ url("/fonts/ElaineSans.svg") format("svg"),
            /* Legacy iOS */ url("/fonts/ElaineSans.ttf") format("truetype"),
            /* Safari, Android, iOS */ url("/fonts/ElaineSans.woff")
              format("woff"),
            /* Modern Browsers */ url("/fonts/ElaineSans.woff2") format("woff2"); /* Modern Browsers */
          font-weight: normal;
          font-style: normal;
        }
        body {
          margin: 0;
          background: ${theme.colors.background};
          color: ${theme.colors.font};
          transition: all 0.15s linear;
          font-family: "ElaineSans";
        }
      `}
    />
  );
}
