import React from "react";
import { ContentContainer } from "../components/container/ContentContainer";

import { H2, ColumnText, Text } from "../components/Text";
import { ContentWrapper } from "../components/container/ContentWrapper";
import { ColumnContent } from "../components/container/ColumnContent";

export default function Payment() {
  return (
    <ContentWrapper>
      <ContentContainer>
        <H2>BEZAHLUNG</H2>
        <Text>
          Idealerweise wären Einkommen und Arbeit voneinander getrennt. Da mir
          bis jetzt allerdings noch kein besserer Weg eingefallen ist um ein
          Einkommen zu generieren, gehe ich erst einmal den "klassischen" Weg.{" "}
          <br />
          Dazu die folgende Frage: <br />
          Was ist Arbeit wert? Bei dieser Frage stehe ich noch ganz am Anfang.
          Mir ist noch nicht richtig klar, wie Preise entstehen. Ein Ansatzpunkt
          ist der Käufer. Er entscheidet, was ihm ein Gerät und die Menschen,
          die mit ihrer Arbeit die Entstehung des Produkts oder der sog.
          "Dienstleistung" ermöglicht haben, wert ist. Ich habe das Gefühl, dass
          diese Frage bei einer Kaufentscheidung maßgeblich ist. Ist mir diese
          Arbeit oder das Produkt 99,95€ wert oder nicht? Ist es mir zu teuer,
          scheint mir diese Arbeit (und die dahinter stehenden Menschen?) das
          nicht wert zu sein.
          <br />
          Genau so gehe ich erstmal an mein Angebot heran und gebe diese Frage
          an Sie weiter: Was ist Ihnen meine Arbeit wert? <br /> <br />
          {/* Vielleicht kann ich Ihnen einen Anhaltspunkt für die Antwort auf diese Frage geben. Dazu gebe ich einen Einblick in meine finanzielle Situation, bringe sozusagen Transparenz in meine monetäre Existenz:  */}
        </Text>
      </ContentContainer>
      <ColumnContent>
        <ColumnText>
          <b>Einnahmen 2020</b> <br />
          bisher keine Aufträge
          <br /> <br />
          <b>Einnahmen 2019</b>
          <br />
          keine Aufträge
          <br />
          <br />
          <b>Einnahmen 2018</b> <br />
          keine Aufträge
          <br /> <br />
          <b>Einnahmen 2017</b> <br />
          Ein Auftrag <br />
          30€
          <br />
          <br />
          <b>Einnahmen 2016</b> <br />
          Drei Aufträge <br />
          136,50€ <br /> <br />
          <b>Einnahmen 2015</b>
          <br />
          Sechs Aufträge
          <br />
          105€
        </ColumnText>
      </ColumnContent>
    </ContentWrapper>
  );
}
