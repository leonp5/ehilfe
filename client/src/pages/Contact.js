import React from "react";

import { H2, H5, ModalText, ModalWarning } from "../components/Text";
import { ContentContainer } from "../components/container/ContentContainer";
import { InputField, TextArea } from "../components/form/Inputs";
import { Form } from "../components/form/Form";
import { Label } from "../components/form/Label";
import { SendButton, CenterSendButton } from "../components/buttons/SendButton";
import useSessionStorage from "../hooks/useSessionStorage";
import sendMail from "../api/sendMail";
import ConfirmationModal from "../components/modals/ConfirmationModal";
import { InlineAnchor, InlineLink } from "../components/navigation/NavLinks";
import CheckBoxContainer from "../components/container/CheckBoxContainer";

export default function Contact() {
  const [success, setSuccess] = React.useState(true);
  const [show, setShow] = React.useState(false);
  const [isChecked, setIsChecked] = React.useState();

  const [request, setRequest] = useSessionStorage("request", {
    name: "",
    email: "",
    message: ""
  });

  function handleChange(event) {
    const value = event.target.value;
    setRequest({
      ...request,
      [event.target.name]: value
    });
  }

  function handleSubmit(event) {
    event.preventDefault();
    sendMail(`/api/request`, request, setShow, setSuccess);
    setRequest({ name: "", email: "", message: "" });
    sessionStorage.removeItem("request");
  }

  return (
    <ContentContainer>
      <Form onSubmit={handleSubmit}>
        <H2>KONTAKT</H2>
        <Label>Name:*</Label>
        <InputField value={request.name} name="name" onChange={handleChange} />
        <Label>e-Mail:*</Label>
        <InputField
          value={request.email}
          name="email"
          onChange={handleChange}
        />
        <Label>Ihr Anliegen, Problem, Frage, etc:*</Label>
        <TextArea
          value={request.message}
          name="message"
          onChange={handleChange}
        />
        <H5>* = Pflichtfeld</H5>

        {CheckBoxContainer(isChecked, () => setIsChecked(!isChecked))}

        <CenterSendButton>
          <SendButton
            disabled={
              !request.name ||
              !request.email ||
              !request.message ||
              isChecked !== true
            }
          >
            Absenden
          </SendButton>
        </CenterSendButton>
        {show && (
          <>
            <ConfirmationModal>
              {success && (
                <ModalText>
                  Deine Nachricht ist angekommen. <br /> Ich melde mich bei dir.{" "}
                  <br /> <br />
                  <InlineLink to="/">Hier gehts zur Startseite</InlineLink>{" "}
                </ModalText>
              )}{" "}
              {!success && (
                <ModalWarning>
                  Deine Nachricht ist leider nicht angekommen. <br /> Du kannst
                  es noch Mal{" "}
                  <InlineAnchor onClick={() => setShow(false)}>
                    probieren
                  </InlineAnchor>{" "}
                  oder mir eine{" "}
                  <InlineAnchor href="mailto:leonpe@web.de">
                    E-Mail
                  </InlineAnchor>{" "}
                  schicken.
                </ModalWarning>
              )}
            </ConfirmationModal>
          </>
        )}
      </Form>
    </ContentContainer>
  );
}
