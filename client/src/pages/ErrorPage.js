import React from "react";

import { H2, Text } from "../components/Text";

export default function ErrorPage() {
  return (
    <>
      <H2>404 Page not found</H2>
      <Text>Diese Seite gibt es nicht.</Text>
    </>
  );
}
