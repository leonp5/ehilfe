import React from "react";

import { ContentContainer } from "../components/container/ContentContainer";
import { H2, Text, ColumnText } from "../components/Text";
import { InlineAnchor } from "../components/navigation/NavLinks";
import { ColumnContent } from "../components/container/ColumnContent";
import { ContentWrapper } from "../components/container/ContentWrapper";

export default function Idea() {
  return (
    <ContentWrapper>
      <ContentContainer>
        <H2>DIE IDEE</H2>
        <Text>
          <b>Über mich</b> <br />
          Mein Name ist Leon Pelzer. Ich bin 1989 geboren und habe 2015 mein
          Bachelorstudium in Philosophie und Politik an der{" "}
          <InlineAnchor
            href="https://www.uni-bielefeld.de/"
            target="_blank"
            rel="noopener"
          >
            Universität Bielefeld
          </InlineAnchor>{" "}
          abgeschlossen. Im Januar 2019 habe ich einen Master an der{" "}
          <InlineAnchor
            href="https://www.cusanus-hochschule.de/"
            target="_blank"
            rel="noopener"
          >
            Cusanus Hochschule
          </InlineAnchor>{" "}
          abgeschlossen. Auf den ersten Blick erscheint mein Studium nicht als
          ideale Voraussetzung für das hier unterbreitete Angebot. Vielleicht
          lesen Sie deshalb erstmal weiter? <br /> <br />
          <b>Die Frage</b> <br />
          Wie so ziemlich jeder Mensch stelle ich mir häufig die Frage, was ich
          in der Zukunft machen möchte. Was interessiert mich? Welche
          Fähigkeiten habe ich? Wie will ich mich in die Gesellschaft
          einbringen? Es ist sozusagen ein Versuch der Selbstbeobachtung und
          Selbstwahrnehmung.
          <br />
          Es war eine Frage der Zeit bis ich mein gesteigertes Interesse an
          Laptops und Smartphones als eine Fähigkeiten von mir entdeckte. Es
          stört mich wenn ich auf ein Problem oder eine Fehlermeldung stoße, die
          mich daran hindert, etwas nach meinen Vorstellungen umzusetzen. Aus
          diesem Grund ist es für mich wichtig, die Geräte und ihre Mechanismen
          bis zu einem gewissen Grad zu verstehen, damit ich sie bedienen kann.
          Diese Geräte sollen hilfreiche Werkzeuge im Alltag sein und mir weder
          Probleme bescheren noch Zeit rauben. Um das zu gewährleisten, muss ich
          natürlich Zeit investieren, in der ich die Bedienung erst einmal
          lerne. Vermutlich ist es bei vielen "Werkzeugen" so. Damit ich die
          Vorzüge eines Autos genießen kann, muss ich erst einmal Zeit in die
          Fahrschule investieren.
          <br />
          Ich habe nun schon viel Zeit in die Suche nach Problemlösungen für
          Computer, Smartphones und Tablets investiert. Diese Recherchen und die
          Selbstbeobachtung führen für mich zu der Frage, ob ich das in die
          Gesellschaft einbringen kann. Kann ich mit dieser Fähigkeit in Zukunft
          etwas tun?
          <br />
          In diesem Sinne etwa ist die gesamte Unternehmung ein Versuch. Eine
          Frage. Ich bin gespannt auf Ihre Antwort.
          <br />
          <br />
          <b>Die Idee</b> <br />
          Die konkrete Idee entstand aus zwei Beobachtungen: zum einen war da
          meine Unzufriedenheit mit der Situation in Elektronikfachgeschäften.
          Eine kurze technische Beratung, die ohne entsprechende Vorkenntnisse
          schnell überfordert. Informiert man sich vorher im Internet über
          bestimmte Produkte, hat man oft fundiertere Kenntnisse über diese
          Produkte als der Verkäufer da dieser ja eine gesamte Abteilung
          betreut. Zumal sich in Frage stellen lässt, ob Ihre optimale Beratung
          oder doch der Verkauf von Geräten im Mittelpunkt steht.
          <br />
          Zum anderen habe ich festgestellt, dass ich im Familien- und
          Freundeskreis immer wieder in Situationen war, in denen ich meinem
          Gegenüber etwas zeigte, erklärte oder ein Computerproblem behob.
          Häufig ging es dabei um die Bedienung von bestimmten Programmen oder
          die Anschaffung eines neuen Gerätes. Ich besitze also nicht nur
          Kenntnisse über die Geräte sondern auch die Fähigkeit und das
          Interesse diese weiterzugeben.
          <br /> <br />
          <b>Was mache ich also?</b>
          <br />
          Ich biete Computerhilfe und Beratung beim Umgang mit elektronischen
          Geräten im. Egal ob es ich um Probleme mit dem Smartphone, dem
          Internet oder dem Laptop handelt.
        </Text>
      </ContentContainer>
      <ColumnContent>
        <ColumnText>
          <b>Die "klassischen Eckdaten"</b> <br /> <br /> 1989 geboren in
          Herdecke
          <br />
          2009 Abitur{" "}
          <InlineAnchor
            href="https://www.rss-witten.de/"
            target="_blank"
            rel="noopener"
          >
            RSS Witten
          </InlineAnchor>
          <br />
          2009-2011 Bundeswehr <br />
          2015 B.A. Philosophie & Politik <br />
          2019 M.A. Philosophie
        </ColumnText>
      </ColumnContent>
    </ContentWrapper>
  );
}
