import React from "react";
import { Text, H2, H3, AdviceText } from "../components/Text";
import { ContentContainer } from "../components/container/ContentContainer";
import { RightImage, LeftImage } from "../components/Images";
import {
  CenterButton,
  ContactButton,
  CenterButtonBottom
} from "../components/buttons/ContactButton";
import { NavLink } from "react-router-dom";
import { ButtonLink } from "../components/navigation/NavLinks";

export default function Home() {
  return (
    <>
      <ContentContainer>
        <H2>DAS ANGEBOT</H2>
        <AdviceText>
          Hinweis: Mittlerweile verfolge ich die Beratung nicht mehr mit der
          anfänglichen Intensität. <br /> Die Website selbst habe ich zu
          Übungszwecken im März 2020 neu entwickelt.
        </AdviceText>
        <Text>
          <RightImage src="/images/rechts.jpg" alt="hold smartphone" />
          Persönliche elektro Hilfe oder kurz: <i>e-Hilfe</i> <br /> Hilfe bei
          Ihren Problemen mit Computer, Smartphone, Tablet & Co. Es geht um den
          alltäglichen Gebrauch Ihrer elektronischen Begleiter. Ob Smartphone,
          Tablet, Laptop oder PC. Sie stellen die Fragen, ich beantworte sie.
          Kein "Fach-Chinesisch", einfache Erklärungen und Beratung. Ich berate
          Sie persönlich, am Telefon oder helfe Ihnen via Fernwartung. <br />{" "}
          Angefangen beim Kauf eines Gerätes bis hin zu komplizierteren Aktionen
          wie Daten-Backups oder Betriebssystemwechsel. Dazwischen ist alles
          möglich:
          <ul>
            <li>
              Umgang mit dem Smartphone oder Tablet lernen: Apps nutzen, Fotos
              machen und auf den Pc kopieren, Emails schreiben u.v.m.
            </li>
            <li>
              Computer/Laptop einrichten, aufräumen oder neu installieren{" "}
            </li>
            <li>Das heimische DSL, Netzwerk oder den Drucker einrichten </li>
            <li>
              Jedwedes Problem mit Smartphone, Tablet oder Notebook beheben{" "}
            </li>
          </ul>
          Ihre Fragen können aus den unterschiedlichsten Motivationen heraus
          entstehen:
          <ul>
            <li>
              Sie fühlen sich bei der Beratung in großen Fachmärkten nicht ernst
              genommen oder es geht Ihnen zu schnell.
            </li>
            <li>
              Sie planen den Ersterwerb eines Gerätes und kennen sich mit der
              Materie noch nicht aus.
            </li>
            <li>
              Sie sind mit Ihren Kenntnissen über die Bedienung Ihrer Geräte
              unzufrieden.{" "}
            </li>
            <li>
              Sie möchten ein neues Gerät kaufen, haben aber weder Zeit noch
              Lust sich mit dem "Fach-Chinesisch" vertraut zu machen um das
              passende Angebot für Sie zu finden.
            </li>
          </ul>
          <CenterButton>
            <NavLink to="/contact">
              <ContactButton>Kontakt</ContactButton>
            </NavLink>
          </CenterButton>
          <br />
        </Text>
      </ContentContainer>
      <ContentContainer>
        <H3>Ganz konkret: Wie kann ich Ihnen helfen?</H3>
        <Text>
          <LeftImage src="/images/links.jpg" alt="laptop" />
          <b>Beispiel</b> <br />
          Sie möchten sich ein neues Gerät kaufen, wissen aber nicht auf was Sie
          beim Kauf achten sollen. Wir vereinbaren einen telefonischen oder
          persönlichen Termin und finden heraus welches Gerät das beste für Sie
          ist. Wir klären dann zum Beispiel Ihre Preisvorstellung, den
          Anwendungsbereich (vor allem bei Laptops/PC's relevant) oder wo sie
          das Gerät am besten kaufen. <br /> <br />
          <b>...oder</b> <br />
          Sie möchten den Umgang mit Ihrem Smartphone/Laptop verbessern. Die
          Bedienung bestimmer Apps oder Programme verstehen Sie nur
          oberflächlich oder gar nicht und wollen das ändern. Wir vereinbaren
          einen Termin und zusammen entwickeln wir einen Weg, der Ihnen die
          Bedienung erleichtert. <br /> <br />
          <b>...oder</b>
          <br />
          Ihr Computer ist über die Jahre langsam geworden und/oder Sie haben
          den Überblick über die installierten und nicht mehr benötigten
          Programme und Dateien verloren? Wir vereinbaren einen Termin und
          bringen Ihren Computer wieder auf Vordermann. Daten-Backup und
          Neuinstallation Ihres Systems sind natürlich auch möglich. <br />{" "}
          <br />
          <b>...oder ...oder ...oder</b> <br />
          Wenn ich Ihr Anliegen, Problem oder Ihre Frage nicht genannt habe,
          scheuen Sie sich nicht mich zu kontaktieren. <br /> <br />
          <CenterButtonBottom>
            <ButtonLink to="/contact">
              <ContactButton>Kontakt</ContactButton>
            </ButtonLink>
          </CenterButtonBottom>
        </Text>
      </ContentContainer>
    </>
  );
}
