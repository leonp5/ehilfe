import React from "react";
import { ThemeProvider } from "emotion-theming";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

import { light, dark } from "./themes/themes";
import GlobalStyles from "./GlobalStyles";
import Header from "./components/container/Header";
import useThemeSwitch from "./hooks/useThemeSwitch";
import { PageContainer } from "./components/container/PageContainer";
import useMountCheck from "./hooks/useMountCheck";
import Home from "./pages/Home";
import Contact from "./pages/Contact";
import Payment from "./pages/Payment";
import Idea from "./pages/Idea";
import ErrorPage from "./pages/ErrorPage";
import { ModalProvider } from "./components/contexts/ToggleModalContext";
import { ToggleThemeContext } from "./components/contexts/ToggleThemeContext";
import { Footer } from "./components/navigation/Footer";
import { Main } from "./components/container/Main";
import Disclaimer from "./components/buttons/Disclaimer";
import BlurredBackground from "./components/container/BlurredBackground";
import BackToTopButton from "./components/buttons/BackToTopButton";
import { ReplaceScrollBar } from "./components/container/ReplaceScrollBar";

export default function App() {
  const [theme, toggleTheme] = useThemeSwitch();
  const activeTheme = theme === "light" ? light : dark;
  const show = useMountCheck();

  return (
    <ThemeProvider theme={activeTheme}>
      <ModalProvider>
        <ToggleThemeContext.Provider value={toggleTheme}>
          <GlobalStyles />
          <PageContainer>
            <Router>
              {show && (
                <BlurredBackground>
                  <Header />
                  <ReplaceScrollBar>
                    <Main>
                      <Switch>
                        <Route exact path="/" component={Home} />
                        <Route exact path="/idea" component={Idea} />
                        <Route exact path="/payment" component={Payment} />
                        <Route exact path="/contact" component={Contact} />
                        <Route path="*" component={ErrorPage} />
                      </Switch>
                    </Main>
                  </ReplaceScrollBar>
                  <Footer />
                  <BackToTopButton />
                </BlurredBackground>
              )}
            </Router>
            <Disclaimer />
          </PageContainer>
        </ToggleThemeContext.Provider>
      </ModalProvider>
    </ThemeProvider>
  );
}
