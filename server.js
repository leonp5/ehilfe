const dotenv = require("dotenv");
dotenv.config();
const express = require("express");
const path = require("path");

const mailRoute = require("./backend/mailRoute");

const app = express();

const PORT = process.env.PORT || 8080;

app.use(express.json({ extended: false }));
app.use(express.urlencoded({ extended: false }));

app.use("/api", mailRoute);

// Serve any static files
app.use(express.static(path.join(__dirname, "client/build")));

// Handle React routing, return all requests to React app
app.get("*", function(req, res) {
  res.sendFile(path.join(__dirname, "client/build", "index.html"));
});

app.listen(PORT, () => {
  console.log(`Server running on Port:${PORT}`);
});
