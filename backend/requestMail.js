const requestMail = request => {
  const mailOptions = {
    from: `${process.env.MAIL}`,
    to: `${process.env.MAIL}`,
    subject: `e-Hilfe Anfrage von ${request.name}`,
    text: `${request.message}\n\nDu kannst ${request.name} unter folgender E-Mailadresse antworten: ${request.email}`
  };
  return mailOptions;
};

module.exports = requestMail;
