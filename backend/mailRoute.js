const express = require("express");
const mailRoute = express.Router();

const requestMail = require("./requestMail");
const sendMail = require("./sendMail");

mailRoute.post("/request", async (request, response) => {
  try {
    const message = request.body;

    const Mail = requestMail(message);
    await sendMail(Mail);
    response.end();
  } catch (error) {
    console.error(error);
    response.status(500).end();
  }
});

module.exports = mailRoute;
